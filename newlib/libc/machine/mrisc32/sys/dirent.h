/*
   dirent types for MRISC32
   Copyright (C) 2024  Marcus Geelnard

   The authors hereby grant permission to use, copy, modify, distribute,
   and license this software and its documentation for any purpose, provided
   that existing copyright notices are retained in all copies and that this
   notice is included verbatim in any distributions. No written agreement,
   license, or royalty fee is required for any of the authorized uses.
   Modifications to this software may be copyrighted by their authors
   and need not follow the licensing terms described here, provided that
   the new terms are clearly indicated on the first page of each file where
   they apply.
*/

#ifndef _SYS_DIRENT_H
#define _SYS_DIRENT_H

#include <sys/types.h>  /* ino_t */

#ifdef __cplusplus
extern "C" {
#endif

#define MAXNAMLEN	255

struct dirent {
  ino_t d_ino;
  char d_name[MAXNAMLEN + 1];
};

typedef struct {
  unsigned int handle;
  struct dirent dirent;
} DIR;

#ifdef __cplusplus
}
#endif

#endif  /* _SYS_DIRENT_H */
