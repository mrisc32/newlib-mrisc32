/* crt1.S -- alternative startup file for MRISC32 (no ctor/dtor support).
 *
 * Copyright (c) 2022  Marcus Geelnard
 *
 * The authors hereby grant permission to use, copy, modify, distribute,
 * and license this software and its documentation for any purpose, provided
 * that existing copyright notices are retained in all copies and that this
 * notice is included verbatim in any distributions. No written agreement,
 * license, or royalty fee is required for any of the authorized uses.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 */

	.file	"crt1.S"

	.globl	_start
	.section .text.start, "ax"
	.p2align 2
	.type	_start,@function
_start:
	; Temporary stack, just for calling memset() and sbrk().
	ldi	sp, #_end+512

	; Clear the bss area. We need to do this before calling any other
	; functions.
	ldi	r1, #__bss_start
	ldi	r3, #__bss_end
	sub	r3, r3, r1
	ldi	r2, #0
	call	memset@pc

	; We use sbrk() to allocate a stack on the heap.
	call    _getstacksize@pc
	mov     r20, r1
	call	sbrk@pc
	add	sp, r1, r20	; Stack top is: _end + STACK_SIZE

	; Collect argc and argv by calling _getarguments().
	add	sp, sp, #-8
	ldea	r1, [sp, #0]	; r1 = &argc
	ldea	r2, [sp, #4]	; r2 = &argv
	call	_getarguments@pc
	ldw	r1, [sp, #0]	; r1 = argc
	ldw	r2, [sp, #4]	; r2 = argv
	add	sp, sp, #8

	; Call main().
	call	main@pc

	; Call exit().
	call	exit@pc

	; Stop (loop forever) if we ever get here.
1:	b	1b

	.size	_start,.-_start

