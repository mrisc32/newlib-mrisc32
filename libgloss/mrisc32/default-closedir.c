/* default-unlink.c -- Default closedir() for MRISC32.
 *
 * Copyright (c) 2024  Marcus Geelnard
 *
 * The authors hereby grant permission to use, copy, modify, distribute,
 * and license this software and its documentation for any purpose, provided
 * that existing copyright notices are retained in all copies and that this
 * notice is included verbatim in any distributions. No written agreement,
 * license, or royalty fee is required for any of the authorized uses.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 */

#include <dirent.h>
#include <sys/errno.h>
#include <sys/unistd.h>
#include "glue.h"

typedef int (*closedir_fptr_t)(DIR*);

static closedir_fptr_t s_closedir_handler;

void
_set_closedir_handler (closedir_fptr_t f)
{
   s_closedir_handler = f;
}

/*
 * closedir -- close a directory stream.
 */
int
closedir (DIR *dirp)
{
  if (s_closedir_handler)
    return s_closedir_handler (dirp);

  errno = EBADF;
  return -1;
}

