/* default-getentropy.c -- Default getentropy() for MRISC32.
 *
 * Copyright (c) 2022  Marcus Geelnard
 *
 * The authors hereby grant permission to use, copy, modify, distribute,
 * and license this software and its documentation for any purpose, provided
 * that existing copyright notices are retained in all copies and that this
 * notice is included verbatim in any distributions. No written agreement,
 * license, or royalty fee is required for any of the authorized uses.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 */

#include <errno.h>
#include <stdint.h>
#include <sys/types.h>
#include <unistd.h>
#include "glue.h"

typedef int (*getentropy_fptr_t)(void*, size_t);

static getentropy_fptr_t s_getentropy_handler;

void
_set_getentropy_handler (getentropy_fptr_t f)
{
  s_getentropy_handler = f;
}


/* This is a simple PRNG based on the PCG family of RNGs.
   See: https://www.pcg-random.org/  */

#define MAX_ENTROPY 0x40000000U

typedef struct { uint64_t state;  uint64_t inc; } pcg32_random_t;

static uint32_t pcg32_random_r(pcg32_random_t* rng)
{
  uint64_t oldstate = rng->state;
  rng->state = oldstate * 6364136223846793005ULL + (rng->inc|1);
  uint32_t xorshifted = ((oldstate >> 18u) ^ oldstate) >> 27u;
  uint32_t rot = oldstate >> 59u;
  return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
}


/*
 * getentropy -- fill a buffer with random bytes.
 */
int
getentropy(void *buf, size_t buflen)
{
  if (s_getentropy_handler)
    return s_getentropy_handler (buf, buflen);

  if (buflen > MAX_ENTROPY)
  {
    errno = -EIO;
    return -1;
  }

  /* The default implementation is just a simple PRNG.
     TODO(m): We should let the initial state depend on some entropy
     sources, such as the current time and static linker addresses.  */
  static pcg32_random_t s_state = { 42U, 54U };
  uint8_t* dst = (uint8_t*)buf;
  for (int bytes_left = (int)buflen; bytes_left > 0; bytes_left -= 4)
  {
    uint32_t four_bytes = pcg32_random_r (&s_state);
    int count = (bytes_left < 4 ? bytes_left : 4);
    for (int j = 0; j < count; ++j)
    {
      *dst++ = (uint8_t)four_bytes;
      four_bytes >>= 8;
    }
  }

  return 0;
}
