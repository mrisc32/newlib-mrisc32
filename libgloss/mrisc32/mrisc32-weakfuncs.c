/* mrisc32-weakfuncs.c -- Weakly linked library functions for MRISC32.
 *
 * Copyright (c) 2022  Marcus Geelnard
 *
 * The authors hereby grant permission to use, copy, modify, distribute,
 * and license this software and its documentation for any purpose, provided
 * that existing copyright notices are retained in all copies and that this
 * notice is included verbatim in any distributions. No written agreement,
 * license, or royalty fee is required for any of the authorized uses.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 */

#include "mrisc32-weakfuncs.h"

/*------------------------------------------------------------------------
 * Implement/override these functions to configure the memory system.
 *------------------------------------------------------------------------*/

size_t __attribute__((weak))
_getstacksize(void)
{
  /* Usa a large stack by default (works well for the simulator).  */
  return (size_t)(1024 * 1024);
}

char* __attribute__((weak))
_getheapend(char* heap_start)
{
  /* By default, allow the allocate to use (almost) the entire 32-bit
     address space (e.g. works with the simulator).  */
  (void)heap_start;
  return (char*)0xffff0000U;  /* 0xffff0000 = start of sim routines  */
}

