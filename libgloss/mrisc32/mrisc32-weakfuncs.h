/* mrisc32-weakfuncs.c -- Weakly linked library functions for MRISC32.
 *
 * Copyright (c) 2022  Marcus Geelnard
 *
 * The authors hereby grant permission to use, copy, modify, distribute,
 * and license this software and its documentation for any purpose, provided
 * that existing copyright notices are retained in all copies and that this
 * notice is included verbatim in any distributions. No written agreement,
 * license, or royalty fee is required for any of the authorized uses.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 */

#ifndef _MRISC32_WEAKFUNCS_H_
#define _MRISC32_WEAKFUNCS_H_

#include <stddef.h>

size_t _getstacksize(void);
char* _getheapend(char* heap_start);

#endif  /* _MRISC32_WEAKFUNCS_H_  */

