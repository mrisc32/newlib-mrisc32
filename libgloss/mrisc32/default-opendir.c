/* default-unlink.c -- Default opendir() for MRISC32.
 *
 * Copyright (c) 2024  Marcus Geelnard
 *
 * The authors hereby grant permission to use, copy, modify, distribute,
 * and license this software and its documentation for any purpose, provided
 * that existing copyright notices are retained in all copies and that this
 * notice is included verbatim in any distributions. No written agreement,
 * license, or royalty fee is required for any of the authorized uses.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 */

#include <dirent.h>
#include <sys/errno.h>
#include <sys/unistd.h>
#include "glue.h"

typedef DIR* (*opendir_fptr_t)(const char *);

static opendir_fptr_t s_opendir_handler;

void
_set_opendir_handler (opendir_fptr_t f)
{
   s_opendir_handler = f;
}

/*
 * opendir -- open directory associated with a path.
 */
DIR*
opendir (const char *dirname)
{
  if (s_opendir_handler)
    return s_opendir_handler (dirname);

  errno = ENOENT;
  return NULL;
}

