/* default-unlink.c -- Default readdir() for MRISC32.
 *
 * Copyright (c) 2024  Marcus Geelnard
 *
 * The authors hereby grant permission to use, copy, modify, distribute,
 * and license this software and its documentation for any purpose, provided
 * that existing copyright notices are retained in all copies and that this
 * notice is included verbatim in any distributions. No written agreement,
 * license, or royalty fee is required for any of the authorized uses.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 */

#include <dirent.h>
#include <sys/errno.h>
#include <sys/unistd.h>
#include "glue.h"

typedef struct dirent* (*readdir_fptr_t)(DIR*);

static readdir_fptr_t s_readdir_handler;

void
_set_readdir_handler (readdir_fptr_t f)
{
   s_readdir_handler = f;
}

/*
 * readdir -- read a directory.
 */
struct dirent*
readdir (DIR *dirp)
{
  if (s_readdir_handler)
    return s_readdir_handler (dirp);

  errno = EBADF;
  return NULL;
}

