/* default-getarguments.c -- Default getarguments().
 *
 * Copyright (c) 2022  Marcus Geelnard
 *
 * The authors hereby grant permission to use, copy, modify, distribute,
 * and license this software and its documentation for any purpose, provided
 * that existing copyright notices are retained in all copies and that this
 * notice is included verbatim in any distributions. No written agreement,
 * license, or royalty fee is required for any of the authorized uses.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 */

typedef void (*getarguments_fptr_t)(int*, char***);

static getarguments_fptr_t s_getarguments_handler;

void
_set_getarguments_handler (getarguments_fptr_t f)
{
   s_getarguments_handler = f;
}

static const char* DEFAULT_ARGV[1] = { "mrisc32" };

/*
 * _getarguments - get argv and argc for the calling process.
 */
void
_getarguments(int* argc, char*** argv)
{
  if (s_getarguments_handler)
    return s_getarguments_handler (argc, argv);

  *argc = 1;
  *argv = (char**)&DEFAULT_ARGV[0];  /* Yes, we cast away the const...  */
}
