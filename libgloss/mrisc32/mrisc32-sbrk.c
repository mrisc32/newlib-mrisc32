/* mrisc32-sbrk.c -- Implementation of sbrk() for MRISC32.
 *
 * Copyright (c) 2020  Marcus Geelnard
 *
 * The authors hereby grant permission to use, copy, modify, distribute,
 * and license this software and its documentation for any purpose, provided
 * that existing copyright notices are retained in all copies and that this
 * notice is included verbatim in any distributions. No written agreement,
 * license, or royalty fee is required for any of the authorized uses.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 */

#include <sys/errno.h>
#include <sys/types.h>
#include "glue.h"
#include "mrisc32-weakfuncs.h"

static
char* align_ptr (char* ptr)
{
  static const unsigned long ALIGNMENT = 8;
  return (char *) ((((unsigned long)ptr) + ALIGNMENT-1) & ~(ALIGNMENT-1));
}


/*
 * _sbrk -- grow the heap. Get nbytes more RAM. We just increment a pointer
 *          in what's left of memory in the system.
 */
caddr_t
_sbrk (size_t nbytes)
{
  char* const HEAP_START = (char *)&_end;
  static char* s_heap_ptr = NULL;
  static char* s_heap_end = NULL;
  char* new_heap_ptr;
  char* allocated_ptr;

  /* Initialize the heap pointer if this is the first call to _sbrk.  */
  if (s_heap_ptr == NULL)
    {
      s_heap_ptr = align_ptr (HEAP_START);
    }

  /* Get the heap end if this is the first call to _sbrk.  */
  if (s_heap_end == NULL)
    {
      s_heap_end = _getheapend (HEAP_START);
    }

  /* Calculcate the next heap pointer and check for OOM.  */
  new_heap_ptr = align_ptr (s_heap_ptr + nbytes);
  if (new_heap_ptr > s_heap_end || new_heap_ptr < s_heap_ptr)
    {
      errno = ENOMEM;
      return (caddr_t) -1;
    }

  /* Commit allocation and return.  */
  allocated_ptr = s_heap_ptr;
  s_heap_ptr = new_heap_ptr;
  return (caddr_t) allocated_ptr;
}

