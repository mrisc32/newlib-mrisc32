/* mrisc32-times.c -- times() for MRISC32.
 *
 * Copyright (c) 2022  Marcus Geelnard
 *
 * The authors hereby grant permission to use, copy, modify, distribute,
 * and license this software and its documentation for any purpose, provided
 * that existing copyright notices are retained in all copies and that this
 * notice is included verbatim in any distributions. No written agreement,
 * license, or royalty fee is required for any of the authorized uses.
 * Modifications to this software may be copyrighted by their authors
 * and need not follow the licensing terms described here, provided that
 * the new terms are clearly indicated on the first page of each file where
 * they apply.
 */

#include <sys/errno.h>
#include <sys/times.h>
#include <time.h>
#include "glue.h"

unsigned long long _gettimemicros (void);

/*
 * _times -- get process times.
 */
clock_t
_times (struct tms *buf)
{
  /* We use _gettimemicros() to get the current time, and assume that
     no time has been spent in system calls nor in child processes.  */
  if (buf)
  {
    const unsigned long long time_us = _gettimemicros ();
    buf->tms_utime = (time_us * CLOCKS_PER_SEC) / 1000000ULL;
    buf->tms_stime = buf->tms_cstime = buf->tms_cutime = 0;
  }
}

